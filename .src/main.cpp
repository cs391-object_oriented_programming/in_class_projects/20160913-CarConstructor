#include <iostream>
using namespace std;


class CarClass {
	public:
		CarClass();	//Default Constructor
		CarClass(string extcolor);	//Constructor that accesses privates
		
		void vCar (string color) {
			cout << "Input the auto Base Price: ";
			cin >> baseprice;
			cout <<	endl <<	baseprice << endl;

			extcolor = color;
			cout << "The car color is: " << extcolor << endl;
			return;
		}

	private:
		double baseprice;
		string extcolor;
};


int main() {
	CarClass *CarObj = new CarClass("");	//initializes values for new object
	string mainColor="";
	
	cin >> mainColor;
	CarObj->vCar(mainColor);
	
	return 0;
}


